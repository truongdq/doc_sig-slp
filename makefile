# 
# This makefile generates a PDF of the MAINTEX file.
# The output can be found inside the build subdirectory.
#
# Makefile based on  http://www.wlug.org.nz/LatexMakefiles
#
#
# This makefile has been tested in Kubuntu 9.04
#
# Prerequisites:
#
# sudo aptitude install ptex-bin ptex-base okumura-clsfiles ptex-jisfonts \
#	 xdvik-ja dvipsk-ja dvi2ps gv jbibtex-bin jmpost mendexk okumura-clsfiles \
#	 vfdata-morisawa5 dvi2ps-fontdesc-morisawa5 texlive-latex-extra latexmk \
#	 dvipng xpdf gs-cjk-resource vfdata-morisawa5 dvi2ps-fontdesc-morisawa5 \ 
#	 cmap-adobe-japan1 cmap-adobe-japan2 cmap-adobe-cns1 cmap-adobe-gb1
# sudo jisftconfig add
#
# When writing Japanese make sure your editor is saving the tex files
# in euc-jp encoding. In VIM this can be accomplished by setting the fenc 
# variable:
#
#  - Open file as normal using vim <filename>
#  - Type  :edit ++enc=euc-jp
#  - Type  :set fenc=euc-jp
#  - Type  :set enc=utf-8 
## TODO Add rule to convert images
## TODO Add makeindex to create .toc files
###########################################################################
## Put here the file name of the main tex file.
MAINTEX	  = JSAISIG
############################################################################
## Change the followin only if you know what you are doing
LATEXCMD	 = platex	 # [latex | platex]
BIBCMD	   = bibtex	# [bibtex | jbibtex]
DVIPDFCMD	= dvipdf	 # [dvipdf | dvipdfm | dvipdfmx]
PDFVIEWER	= okular	   # [okular | xpdf | evince ]
DVIVIEWER	= xdvi-ja	# [xdvi | xdvi-ja | kdvi ]

## No need to change anything below this line
TEXFILES	 = $(wildcard *.tex)
TEXINPUTS=:$(PWD)//:	# Path to search for .tex, .cls and .sty files
BSTINPUTS=:$(PWD)//:	# Path to search for .bst files
BIBINPUTS=:$(PWD)//:	# Path to search for .bib files
TEXMFOUTPUT=$(PWD)/build  # Output dir for bibtex and jbibtex

LATEXOPTS= -output-directory=build -file-line-error -interaction=nonstopmode
export TEXFILES TEXINPUTS BSTINPUTS BIBINPUTS TEXMFOUPUT
.PHONY: all wordcount charcount pdf dvi

all: build/$(MAINTEX).pdf
build :
	@echo "Creating build directory"
	@mkdir build
build/$(MAINTEX).aux: $(MAINTEX).tex $(TEXFILES) build
		$(LATEXCMD) $(LATEXOPTS) $(MAINTEX) 
build/$(MAINTEX).bbl: build/$(MAINTEX).aux
		$(BIBCMD) build/$(MAINTEX)
build/$(MAINTEX).dvi: build/$(MAINTEX).bbl
	$(LATEXCMD) $(LATEXOPTS) $(MAINTEX) 
	$(LATEXCMD) $(LATEXOPTS) $(MAINTEX) 
build/$(MAINTEX).pdf: build/$(MAINTEX).dvi
	$(DVIPDFCMD) build/$(MAINTEX).dvi build/$(MAINTEX).pdf
dvi: build/$(MAINTEX).dvi
	$(DVIVIEWER) build/$(MAINTEX).dvi 
pdf: build/$(MAINTEX).pdf
	$(PDFVIEWER) build/$(MAINTEX).pdf 
# Word counting can be done in VIM usng Ctrl-g g but this command also includes
# latex commands. This make rule strips the latex and counts what is left
#
clean:
	rm -f *.dvi *.log *.ind *.aux *.toc *.syn *.idx *.out *.ilg *.pla *.bb
wordcount:
	@echo Approximate word count: `grep -v '^\\\\' $(TEXFILES)|grep -v '^%'|wc -w`
charcount:
	@echo Approximate char count: `grep -v '^\\\\' $(TEXFILES)|grep -v '^%'|wc -c`
